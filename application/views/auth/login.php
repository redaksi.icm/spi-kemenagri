<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>
        <?php echo $title; ?>
    </title>

    <!-- General CSS Files -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!-- CSS Libraries -->
    <link rel="stylesheet" href="<?php echo base_url('/node_modules/bootstrap-social/bootstrap-social.css') ?>">

    <!-- Template CSS -->
    <link rel="stylesheet" href="<?php echo base_url('/assets/css/style.css') ?> ">
    <link rel="stylesheet" href="<?php echo base_url('/assets/css/components.css') ?>">
</head>


<style>
    .card.card-primary {
        border-top: 5px solid #FFF500;
    }

    .card .card-header h4 {
        font-size: 20px;
        line-height: 28px;
        color: #00923F;
        padding-left: 40px;
        padding-right: 40px;
        text-align: center;
        
    }

    .rounded-circle {
        border-radius: 11% !important;
    }

    .login-brand {

        padding-top: 10px;
        padding-left: 50px;
        padding-right: 50px;
        margin-bottom: 4px
        
    }
    

</style>
 <body style="background-image: url('assets/img/kemenaggedung.jpg'); background-size:cover; ">
 
    <div id="app">
        <section class="section">
            <div class="container mt-5">
                <div class="row">
                    <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">


                        <div class="card card-primary">
                            <br>
                            <div style='text-align: center;'><span style='font-size: 28px; color:green;'><span style='font-family: Traditional Arabic;'> ‏اَلسَّلَامُ عَلَيْكُم وَرَحْمَةُ اَللهِ وَبَرَكاتُهُ‎</span></span> </div>
                            <div class="login-brand">
                                <img src="<?php echo base_url('assets') ?>/img/KEMENAG.png" alt="logo" width="100" class="shadow-light rounded-circle">
                            </div>
                            
                            
                            <div class="card-header">

                                <h4>SISTEM PENDATAAN COMMENT</h4>

                            </div>
                            

                            <div class="card-body">
                                <form method="POST" action="<?php echo base_url('LoginController/login'); ?>" class="needs-validation" novalidate="">
                                    <div class="form-group">
                                        <label for="email">Username</label>
                                        <input id="username" class="form-control" name="username" tabindex="1" required autofocus>
                                        <div class="invalid-feedback">
                                            Silahkan isikan Username
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="d-block">
                                            <label for="password" class="control-label">Password</label>
                                            <div class="float-right">
                                            </div>
                                        </div>
                                        <input id="password" type="password" class="form-control" name="password" tabindex="2" required>
                                        <div class="invalid-feedback">
                                            Silahkan isi password
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-block" id="btn-login" tabindex="4" style="background-color:#00923F; color:white; font-size:15px;">
                                            LOGIN
                                        </button>
                                    </div>
                                </form>
                                <div class="simple-footer" style="color: black;">
                            Made with 💝 by CHABELLA &copy; <?php echo date('Y'); ?>
                        </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- General JS Scripts -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="<?php echo base_url('/assets/js/stisla.js') ?>"></script>

    <!-- JS Libraies -->

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <!-- Template JS File -->

    <script src="<?php echo base_url('/assets/js/scripts.js') ?>"></script>
    <script src="<?php echo base_url('/assets/js/custom.js') ?>"></script>

    <!-- Page Specific JS File -->

</body>

</html>