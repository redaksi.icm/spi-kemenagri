<footer class="main-footer">
    <div class="footer-left">
        Copyright &copy; 2019 <div class="bullet"></div> Design By <a href="#">Reza Margareta made with ❤</a>
    </div>
    <div class="footer-right">
        2.3.0
    </div>
</footer>
</div>
</div>

<!-- General JS Scripts -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="<?php echo base_url('assets') ?>/js/stisla.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>


<!-- JS Libraies -->

<script src="<?php echo base_url('node_modules') ?>/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url('node_modules') ?>/chart.js/dist/Chart.min.js"></script>
<script src="<?php echo base_url('node_modules') ?>/owl.carousel/dist/owl.carousel.min.js"></script>
<script src="<?php echo base_url('node_modules') ?>/summernote/dist/summernote-bs4.js"></script>
<script src="<?php echo base_url('node_modules') ?>/chocolat/dist/js/jquery.chocolat.min.js"></script>



<script src="<?php echo base_url('node_modules') ?>/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url('node_modules') ?>/datatables.net-select-bs4/js/select.bootstrap4.min.js"></script>
<script src="<?php echo base_url('node_modules') ?>/prismjs/prism.js"></script>

<!-- Template JS File -->
<script src="<?php echo base_url('assets') ?>/js/scripts.js"></script>
<script src="<?php echo base_url('assets') ?>/js/custom.js"></script>

<!-- Page Specific JS File -->
<script src="<?php echo base_url('assets') ?>/js/page/index.js"></script>
<script src="<?php echo base_url('assets') ?>/js/page/modules-datatables.js"></script>
<script src="<?php echo base_url('assets') ?>/js/page/bootstrap-modal.js"></script>


<script type="text/javascript">
    $('#logout').on('click', function(e) {


        Swal.fire({
            title: 'Confirm Logout?',
            text: "Anda akan keluar dari halaman ini",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                Swal.fire(
                    'Logout',
                    'Anda keluar dari page ini.',
                    'success'
                ).then(() => {
                    window.location.href = '<?php echo base_url('LoginController') ?>'
                })
            }
        })

    });


    
</script>



</body>

</html>