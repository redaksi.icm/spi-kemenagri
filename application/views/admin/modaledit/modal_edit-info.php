<?php
$this->load->view('templates/dashboard_header');
?>

<?php
$this->load->view('admin/templates/admin_sidebar');
?>



<div class="modal fade" tabindex="-1" role="dialog" id="modal_edit_mhs">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Mahasiswa</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>


            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="profil-tab" data-toggle="tab" href="#profil" role="tab" aria-controls="profil" aria-selected="true">Profil</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Edit Data</a>
                </li>
                <!-- <li class="nav-item">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Contact</a>
                </li> -->
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="profil" role="tabpanel" aria-labelledby="profil-tab">

                    <div class="card">
                        <div class="card-body">
                            <div class="section-title mt-0" style="text-align: center">Informasi Lengkap Data Mahasiswa</div>

                            <div class="form-group">
                                <label>Nim</label>
                                <span type="" class="form-control" readonly="" id="nim_info">
                            </div>

                            <div class="form-group">
                                <label>Nama</label>
                                <span type="" class="form-control" readonly="" id="nama_info">
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <span type="" class="form-control" readonly="" id="email_info">
                            </div>
                            <div class="form-group">
                                <label>Alamat</label>
                                <span type="" class="form-control" readonly="" id="alamat_info">
                            </div>
                            <div class="form-group">
                                <label>Jenis Kelamin</label>
                                <span type="" class="form-control" readonly="" id="gender_info">
                            </div>
                            <div class="form-group">
                                <label>Tempat Lahir</label>
                                <span type="" class="form-control" readonly="" id="tempat_lahir_info">
                            </div>
                            <div class="form-group">
                                <label>Tanggal Lahir</label>
                                <span type="" class="form-control" readonly="" id="tgl_lahir_info">
                            </div>
                            <div class="form-group">
                                <label>Nomor Telp</label>
                                <span type="" class="form-control" readonly="" id="no_telp_info">
                            </div>
                            <div class="form-group">
                                <label>Agama</label>
                                <span type="" class="form-control" readonly="" id="agama_info">
                            </div>
                            <div class="form-group">
                                <label>Kewarganegaraan</label>
                                <span type="" class="form-control" readonly="" id="kewarganegaraan_info">
                            </div>

                        </div>
                    </div>
                </div>


                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">

                    <form method="POST" action="<?php echo base_url('Admin/updateMhs') ?>">

                        <input type="hidden" name="id_mhs" id="id_mhs">
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Nim</label>
                                <input type="text" class="form-control" name="nim" id="nim">
                            </div>
                            <div class="form-group">
                                <label>Nama</label>
                                <input type="text" class="form-control" name="nama" id="nama">
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" class="form-control" name="email" id="email">
                            </div>
                            <div class="form-group">
                                <label>Alamat</label>
                                <input type="text" class="form-control" name="alamat" id="alamat">
                            </div>
                            <div class="form-group">
                                <label>Jenis Kelamin</label>
                                <input type="text" class="form-control" name="jenis_kelamin" id="jenis_kelamin">
                            </div>
                            <div class="form-group">
                                <label>Tempat Lahir</label>
                                <input type="text" class="form-control" name="tempat_lahir" id="tempat_lahir">
                            </div>
                            <div class="form-group">
                                <label>Tanggal Lahir</label>
                                <input type="date" class="form-control" name="tgl_lahir" id="tgl_lahir">
                            </div>
                            <div class="form-group">
                                <label>No Telp</label>
                                <input type="text" class="form-control" name="no_telp" id="no_telp">
                            </div>
                            <div class="form-group">
                                <label>Agama</label>
                                <input type="text" class="form-control" name="agama" id="agama">
                            </div>
                            <div class="form-group">
                                <label>Kewarganegaraan</label>
                                <input type="text" class="form-control" name="kewarganegaraan" id="kewarganegaraan">
                            </div>
                        </div>
                        <div class="modal-footer bg-whitesmoke br">

                            <button type="submit" class="btn btn-success">Simpan</button>
                            <button type="button" class="btn btn-warning" data-dismiss="modal">Batal</button>

                        </div>
                    </form>


                </div>
                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">...</div>
            </div>







        </div>



    </div>
</div>






</div>


<!-- General JS Scripts -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="<?php echo base_url('assets') ?>/js/stisla.js"></script>

<!-- JS Libraies -->
<script src="<?php echo base_url('node_modules') ?>/prismjs/prism.js"></script>

<!-- Template JS File -->
<script src="<?php echo base_url('assets') ?>/js/scripts.js"></script>
<script src="<?php echo base_url('assets') ?>/js/custom.js"></script>

<!-- Page Specific JS File -->
<script src="<?php echo base_url('assets') ?>/js/page/bootstrap-modal.js"></script>