<?php
$this->load->view('templates/dashboard_header');
?>

<?php
$this->load->view('templates/dashboard_navbar');
?>

<?php
$this->load->view('admin/templates/admin_sidebar');
?>

<?php

include 'modaledit/modal_editAdmin.php';
?>

<?php

include 'modaltambah/modal_tambahAdmin.php';
?>

<style>
    div#table-1_filter {
        text-align: right;
    }

    .modal-backdrop {
        display: none;
    }
</style>

<body>
    <div id="app">
        <div class="main-wrapper">

            <!-- Main Content -->
            <div class="main-content">
                <section class="section">
                    <div class="section-header">
                        <h1>Data Admin</h1>
                        <h style="margin: 0 auto;" ></h><b id="datetime" style="color: green;"></b>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <!-- <h4>Data Admin</h4> -->
                                    <p class="text-muted font-13 m-b-30" style="position: absolute; right:16px">
                                        <button id="addBtn" style="width:133px;" class="btn btn-success icon-left btn-block" data-toggle="modal" data-target="#tambah_Modal_adm"> <i class="fas fa-plus-circle"></i> Tambah Data </button>
                                    </p>
                                </div>
                                <div class="card-body">

                                    <!-- <p class="text-muted font-13 m-b-30">
                                        <button id="addBtn" style="width:120px;" class="btn btn-success icon-left btn-block" data-toggle="modal" data-target="#tambah_Modal_adm"> Tambah Data </button>
                                    </p> -->
                                    <div class="table-responsive">
                                        <?php if (!empty($admin)) { ?>
                                            <table class="table table-striped" id="table-1" style="text-align: center">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">No. </th>
                                                        <th>Nama Admin</th>
                                                        <th>Username</th>
                                                        <th>Email</th>
                                                        <th>Aksi</th>

                                                        <!-- <th>Status</th>
                                                        <th>Action</th> -->
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $no = 1;
                                                    foreach ($admin as $row) { ?>
                                                        <tr>
                                                            <td class="text-center" width="10%"><?php echo $no++ ?></td>
                                                            <td class="font-w600"><?php echo $row['nama'] ?></td>
                                                            <td class="font-w600"><?php echo $row['username'] ?></td>
                                                            <td class="font-w600"><?php echo $row['email'] ?></td>
                                                            <td>
                                                                <a href="#" data-id="<?= $row['id'] ?>" data-nama="<?= $row['nama'] ?>" data-username="<?= $row['username'] ?>" data-email="<?= $row['email'] ?>" class="btn btn-icon icon-left btn-primary" style="border-radius: 30px" data-toggle="modal" data-target="#modal_edit_admin">
                                                                    <i class=" far fa-edit"></i> Edit
                                                                </a>


                                                                <!-- <a href="#" class="btn btn-icon icon-left btn-danger"><i class="fas fa-times"></i> Hapus</a> -->

                                                                <a href="<?= base_url('Admin/hapusAdmin/' . $row['id']) ?>" class="hapus">
                                                                    <button class="btn btn-icon icon-left btn-danger" type="button" data-toggle="tooltip" style="border-radius: 30px"><i class="fas fa-times"></i>Hapus
                                                                    </button>
                                                                </a>
                                                            </td>


                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Advanced Table</h4>
                                </div>
                                <div class="card-body">

                                    <div class="table-responsive">
                                        <table class="table table-striped" id="table-2">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">
                                                        <div class="custom-checkbox custom-control">
                                                            <input type="checkbox" data-checkboxes="mygroup" data-checkbox-role="dad" class="custom-control-input" id="checkbox-all">
                                                            <label for="checkbox-all" class="custom-control-label">&nbsp;</label>
                                                        </div>
                                                    </th>
                                                    <th class="text-center">No. </th>
                                                    <th>Nama Admin</th>
                                                    <th>Username</th>
                                                    <th>Email</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    <?php } ?>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- </div> -->
                </section>
            </div>

        </div>
    </div>

    



    <!-- JS Libraies -->
    <script src="<?php echo base_url('node_modules') ?>/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('node_modules') ?>/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    

    <!-- js plugin -->
    <!-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script> -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script>
var dt = new Date();
document.getElementById("datetime").innerHTML = dt.toLocaleString();
</script>


    <script type="text/javascript">
        // data tabel
        // $(document).ready(function() {
        //     $('#table-1').DataTable();
        // });



        // tableajax
        $("#table-1").dataTable();

        // $('#exampleModal').on('show.bs.modal', function(e) {

        //     var div = $(e.relatedTarget);

        //     var id = div.data('id');
        //     var modal = $(this);
        //     modal.find('#id').attr("value", id);
        // });



        // swal hapus
        $('.hapus').on("click", function(e) {
            e.preventDefault();
            var url = $(this).attr('href');
            Swal.fire({
                title: 'Anda Yakin?',
                text: "Data tidak bisa di kembalikan lagi!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Hapus !'
            }).then((result) => {
                if (result.value) {
                    Swal.fire(
                        'Deleted!',
                        'Data Berhasil Dihapus.',
                        'success'
                    ).then(() => {
                        window.location.href = url
                    });
                }
            })
        });


        // moda edit method
        $('#modal_edit_admin').on('show.bs.modal', function(e) {

            var div = $(e.relatedTarget);

            var id = div.data('id');
            var nama = div.data('nama');
            var username = div.data('username');
            var email = div.data('email');

            var modal = $(this);
            modal.find('#id').attr("value", id);
            modal.find('#nama').attr("value", nama);
            modal.find('#username').attr("value", username);
            modal.find('#email').attr("value", email);
        });
    </script>



    <?php

    $this->load->view('templates/dashboard_footer');

    ?>