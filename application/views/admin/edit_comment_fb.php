<?php
$this->load->view('templates/dashboard_header');
?>

<?php
$this->load->view('templates/dashboard_navbar');
?>

<?php
$this->load->view('admin/templates/admin_sidebar');
?>

<style>
    .modal-backdrop {
        display: none;
    }
</style>

<?php

include 'modaledit/modal_editComment_FB.php';
?>


<?php

include 'modaltambah/modal_tambahCommentFB.php';
?>


<body>
    <div id="app">
        <div class="main-wrapper">

            <!-- Main Content -->
            <div class="main-content">
                <section class="section">
                    <div class="section-header">
                        <h1>
                            Data Comment Facebook
                        </h1>
                        <h style="margin: 0 auto;" ></h><b id="datetime" style="color: green;"></b>
                    </div>

                    <!-- <div class="section-body">
                        <h2 class="section-title">DataTables</h2>
                        <p class="section-lead">
                            We use 'DataTables' made by @SpryMedia. You can check the full documentation <a href="https://datatables.net/">here</a>.
                        </p> -->

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <!-- <h4>Data Mahasiswa</h4> -->
                                    <p class="text-muted font-13 m-b-30" style="position: absolute; right:16px">
                                        <button id="addBtn" style="width:133px; margin-top: 25px;" class="btn btn-success icon-left btn-block" data-toggle="modal" data-target="#tambah_Modal_CommentFB"> <i class="fas fa-plus-circle"></i> Tambah Data </button>


                                        <a href="<?= base_url('Admin/ExportExcelFB') ?>">
                                        <button id="addBtn" style="width:133px; margin-top: 6px;" class="btn btn-warning icon-left btn-block" data-toggle="modal" data-target="#"> <i class="fas fa-plus-circle"></i> Export Data </button>
                                        </a>
                                   
                                    </p>
                                </div>

                                <div class="card-body">
                                    <div class="table-responsive">
                                        <?php if (!empty($tb_facebook_acc)) { ?>
                                            <table class="table table-striped" id="table-1">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">No. </th>
                                                        <th>account</th>
                                                        <th>Alamat</th>
                                                        <th>No Telp</th>
                                                        <!-- <th>Pertanyaan</th>
                                                        <th>Tanggapan</th> -->
                                                        <th>Aksi</th>

                                                        <!-- <th>Status</th>
                                                        <th>Action</th> -->
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $no = 1;
                                                    foreach ($tb_facebook_acc as $row) { ?>
                                                        <tr>
                                                            <td class="text-center" width="10%"><?php echo $no++ ?></td>
                                                            <!-- <td class="font-w600"><?php echo $row['id_data_fb'] ?></td> -->
                                                            <td class="font-w600"><?php echo $row['account'] ?></td>
                                                            <td class="font-w600"><?php echo $row['alamat'] ?></td>
                                                            <td class="font-w600"><?php echo $row['no_telp'] ?></td>
                                                            
                                                            <td>
                                                                <!-- <div style="margin: 10px 0px;"> -->
                                                                <a href="#" data-id_data_fb="<?= $row['id_data_fb'] ?>" data-account="<?= $row['account'] ?>" data-alamat="<?= $row['alamat'] ?>" data-no_telp="<?= $row['no_telp'] ?>" data-pertanyaan="<?= $row['pertanyaan'] ?>" data-tanggapan="<?= $row['tanggapan'] ?>" style="border-radius: 30px" class="btn btn-icon icon-left btn-warning" data-toggle="modal" data-target="#modal_edit_comment_facebook">
                                                                    <i class=" far fa-edit"></i>
                                                                    Detail
                                                                </a>
                                                                <!-- <a href="#" class="btn btn-icon icon-left btn-danger"><i class="fas fa-times"></i> Hapus</a></td> -->


                                                                <a href="<?= base_url('Admin/hapusCommentFB/' . $row['id_data_fb']) ?>" class="hapus">
                                                                    <button class="btn btn-icon icon-left btn-danger" type="button" data-toggle="tooltip" style="border-radius: 30px;">
                                                                        <i class=" fas fa-times"></i>
                                                                        Hapus
                                                                    </button>
                                                                </a>

                                                                <!-- </div> -->
                                                            </td>


                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } else { ?>
                    <!-- <div class="row"> -->
                    <!-- <div class="col-12"> -->
                    <!-- <div class="card"> -->
                    <!-- <div class="card-header">
                        <h4>Advanced Table</h4>
                    </div> -->
                    <!-- <div class="card-body"> -->

                    <!-- <div class="table-responsive"> -->
                    <!-- <table class="table table-striped" id="table-2">
                            <thead>
                                <tr>
                                    <th class="text-center">
                                        <div class="custom-checkbox custom-control">
                                            <input type="checkbox" data-checkboxes="mygroup" data-checkbox-role="dad" class="custom-control-input" id="checkbox-all">
                                            <label for="checkbox-all" class="custom-control-label">&nbsp;</label>
                                        </div>
                                    </th>
                                    <th class="text-center">No. </th>
                                    <th>Nim</th>
                                    <th>Nama Mahasiswa</th>
                                    <th>Email</th>
                                    <th>Alamat</th>
                                    <th>Jenis Kelamin</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table> -->
                <?php } ?>


                <!-- </div> -->
                <!-- </div> -->
                <!-- </div> -->
                <!-- </div> -->
                <!-- </div> -->
                <!-- </div> -->
                </section>
            </div>

        </div>
    </div>

     <!-- JS Libraies -->
     <script src="<?php echo base_url('node_modules') ?>/datatables/media/js/jquery.dataTables.min.js"></script>
     <script src="<?php echo base_url('node_modules') ?>/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    
     <script>
var dt = new Date();
document.getElementById("datetime").innerHTML = dt.toLocaleString();
</script>
    
    <script>
        $("#table-1").dataTable();



        // swal 
        $('.hapus').on("click", function(e) {
            e.preventDefault();
            var url = $(this).attr('href');
            Swal.fire({
                title: 'Anda Yakin?',
                text: "Data tidak bisa di kembalikan lagi!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    Swal.fire(
                        'Deleted!',
                        'Data Berhasil Dihapus.',
                        'success'
                    ).then(() => {
                        window.location.href = url
                    });
                }
            })
        });



        $('#modal_edit_comment_facebook').on('show.bs.modal', function(e) {

            var div = $(e.relatedTarget);

            var id = div.data('id_data_fb');
            var account = div.data('account');
            var alamat = div.data('alamat');
            var no_telp = div.data('no_telp');
            var pertanyaan = div.data('pertanyaan');
            var tanggapan = div.data('tanggapan');


            var modal = $(this);
            modal.find('#id_data_fb').attr("value",id);
            modal.find('#account').attr("value",account);
            modal.find('#alamat').attr("value",alamat);
            modal.find('#no_telp').attr("value",no_telp);
            modal.find('#pertanyaan').attr("value",pertanyaan);
            modal.find('#tanggapan').attr("value",tanggapan);


            modal.find('#pertanyaan_info').text(pertanyaan);
            modal.find('#tanggapan_info').text(tanggapan);
            
            modal.find('#pertanyaan').text(pertanyaan);
            modal.find('#tanggapan').text(tanggapan);
            
        });
    </script>

    <?php

    $this->load->view('templates/dashboard_footer');

    ?>