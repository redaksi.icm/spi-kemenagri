<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{



    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/m_admin', 'm_admin');
    }


    public function index()
    {
        $data['title'] = 'Administrators';

        // $data['user'] = $this->db->get_where('admin', ['username' => $this->session->userdata('username')])->row_array();
        $data['admin'] = $this->m_admin->show_Admin();
        $data['total_asset'] = $this->m_admin->hitungJumlahAssetMhs();
        $data['total_admin'] = $this->m_admin->hitungJumlahAssetAdmin();
        $data['total_IG'] = $this->m_admin->hitungJumlahAssetIG();
        $data['total_FB'] = $this->m_admin->hitungJumlahAssetFB();
        $data['total_TW'] = $this->m_admin->hitungJumlahAssetTwitter();

        $data['tabel_log'] = $this->m_admin->show_log();



        // $data['total_dosen'] = $this->m_admin->hitungJumlahAssetDosen();
        // $data['total_buku'] = $this->m_admin->hitungJumlahAssetBuku();
        



        $this->load->view('admin/dashboard_admin', $data);
    }

    public function tampilprofile()
    {

        $data['title'] = 'Profil User';
        
        $this->load->view('admin/profile_user', $data);
    }



    public function tampildataAdmin()
    {

        $data['title'] = 'Edit Admin';

        $data['admin'] = $this->m_admin->show_Admin();

        $this->load->view('admin/edit_admin', $data);
    }



    public function tampildataMahasiswa()
    {

        $data['title'] = 'Edit Mahasiswa';


        $data['mahasiswa'] = $this->m_admin->show_Mahasiswa();

        $this->load->view('admin/edit_mahasiswa', $data);
    }


    public function tampildataCommentIG(){

        $data['title'] = 'Edit Data';


        $data['tb_instagram_acc'] = $this->m_admin->show_CommentIG();

        $this->load->view('admin/edit_comment_ig', $data);
    }

    public function tampildataCommentFB(){

        $data['title'] = 'Edit Data';
        $data['tb_facebook_acc'] = $this->m_admin->show_CommentFB();

        $this->load->view('admin/edit_comment_fb', $data);
    }

    public function tampildataCommentTwitter(){

        $data['title'] = 'Edit Data';
        $data['tb_twitter_acc'] = $this->m_admin->show_CommentTwitter();

        $this->load->view('admin/edit_comment_Twt', $data);
    }


    public function updateAdmin()
    {

        $id = $this->input->post('id');
        $data['nama'] = $this->input->post('nama');
        $data['username'] = $this->input->post('username');
        $data['email'] = $this->input->post('email');

        $this->m_admin->update_admin($data, $id);

        log_helper("edit", "Mengedit data admin");

        redirect('Admin/tampildataAdmin');
    }

    public function updateIG()
    {
        $id = $this->input->post('id_data_ig');
        $data['pertanyaan'] = $this->input->post('pertanyaan');
        $data['tanggapan'] = $this->input->post('tanggapan');

        $this->m_admin->update_comment_ig($data, $id);

        log_helper("edit", "Mengedit data Instagram");

        redirect('Admin/tampildataCommentIG');
    }

    public function updateFB()
    {
        $id = $this->input->post('id_data_fb');
        $data['pertanyaan'] = $this->input->post('pertanyaan');
        $data['tanggapan'] = $this->input->post('tanggapan');

        $this->m_admin->update_comment_fb($data, $id);

        log_helper("edit", "Mengedit data Facebook");

        redirect('Admin/tampildataCommentFB');
    }

    public function updateTwitter()
    {
        $id = $this->input->post('id_data_twitter');
        $data['pertanyaan'] = $this->input->post('pertanyaan');
        $data['tanggapan'] = $this->input->post('tanggapan');

        $this->m_admin->update_comment_tw($data, $id);

        log_helper("edit", "Mengedit data Twitter");

        redirect('Admin/tampildataCommentTwitter');
    }

    public function updateMhs()
    {

        $id = $this->input->post('id_mhs');
        $data['nim'] = $this->input->post('nim');
        $data['nama'] = $this->input->post('nama');
        $data['email'] = $this->input->post('email');
        $data['alamat'] = $this->input->post('alamat');
        $data['jenis_kelamin'] = $this->input->post('jenis_kelamin');
        $data['tempat_lahir'] = $this->input->post('tempat_lahir');
        $data['tgl_lahir'] = $this->input->post('tgl_lahir');
        $data['no_telp'] = $this->input->post('no_telp');
        $data['agama'] = $this->input->post('agama');
        $data['kewarganegaraan'] = $this->input->post('kewarganegaraan');

        $this->m_admin->update_Mhs($data, $id);

        log_helper("edit", "Mengedit data mahasiswa");

        redirect('Admin/tampildataMahasiswa');
    }

    public function hapusAdmin($id)
    {

        $this->m_admin->deleteAdmin($id);

        log_helper("delete", "Menghapus data admin");

        redirect('Admin/tampildataAdmin');
    }

    public function hapusMahasiswa($id)
    {

        $this->m_admin->deleteMhs($id);

        redirect('Admin/tampildataMahasiswa');
    }

    public function hapusCommentIG($id)
    {

        $this->m_admin->deleteCommentIG($id);

        log_helper("delete", "Menghapus comment Instagram");

        redirect('Admin/tampildataCommentIG');
    }

    public function hapusCommentFB($id)
    {

        $this->m_admin->deleteCommentFB($id);

        log_helper("delete", "Menghapus comment Facebook");

        redirect('Admin/tampildataCommentFB');
    }
    

    public function hapusCommentTwitter($id)
    {

        $this->m_admin->deleteCommentTwitter($id);

        log_helper("delete", "Menghapus comment Twitter");

        redirect('Admin/tampildataCommentTwitter');
    }

    public function simpanAdmin()
    {
        $data['id'] = $this->input->post('id');
        $data['nama'] = $this->input->post('nama');
        $data['email'] = $this->input->post('email');
        $data['username'] = $this->input->post('username');
        $data['password'] = $this->input->post('password');
        $data['role_id'] = $this->input->post('role_id');

        $this->m_admin->createAdmin($data);

        log_helper("add", "Menambah data admin");

        redirect('Admin/tampildataAdmin');
    }

    public function simpanCommentIG()
    {
        $data['id_data_ig'] = $this->input->post('id_data_ig');
        $data['account'] = $this->input->post('account');
        $data['alamat'] = $this->input->post('alamat');
        $data['no_telp'] = $this->input->post('no_telp');
        $data['pertanyaan'] = $this->input->post('pertanyaan');
        $data['tanggapan'] = $this->input->post('tanggapan');
        $data['tanggal_comment'] = $this->input->post('tanggal_comment');
        

        $this->m_admin->createCommentIG($data);

        log_helper("add", "Menambah data comment Instagram");

        redirect('Admin/tampildataCommentIG');

    }

    public function simpanCommentFB()
    {
        $data['id_data_fb'] = $this->input->post('id_data_fb');
        $data['account'] = $this->input->post('account');
        $data['alamat'] = $this->input->post('alamat');
        $data['no_telp'] = $this->input->post('no_telp');
        $data['pertanyaan'] = $this->input->post('pertanyaan');
        $data['tanggapan'] = $this->input->post('tanggapan');
        $data['tanggal_comment'] = $this->input->post('tanggal_comment');
        

        $this->m_admin->createCommentFB($data);

        log_helper("add", "Menambah data comment Facebook");

        redirect('Admin/tampildataCommentFB');

    }

    public function simpanCommentTwitter()
    {
        $data['id_data_twitter'] = $this->input->post('id_data_twitter');
        $data['account'] = $this->input->post('account');
        $data['alamat'] = $this->input->post('alamat');
        $data['no_telp'] = $this->input->post('no_telp');
        $data['pertanyaan'] = $this->input->post('pertanyaan');
        $data['tanggapan'] = $this->input->post('tanggapan');
        $data['tanggal_comment'] = $this->input->post('tanggal_comment');
        

        $this->m_admin->createCommentTwitter($data);
        log_helper("add", "Menambah data comment Twitter");

        redirect('Admin/tampildataCommentTwitter');

    }


    public function simpanMhs()
    {

        $data['id_mhs'] = $this->input->post('id_mhs');
        $data['nim'] = $this->input->post('nim');
        $data['nama'] = $this->input->post('nama');
        $data['alamat'] = $this->input->post('alamat');
        $data['no_telp'] = $this->input->post('no_telp');
        $data['jenis_kelamin'] = $this->input->post('jenis_kelamin');
        $data['tempat_lahir'] = $this->input->post('tempat_lahir');
        $data['kewarganegaraan'] = $this->input->post('kewarganegaraan');
        $data['agama'] = $this->input->post('agama');
        $data['tgl_lahir'] = $this->input->post('tgl_lahir');
        $data['email'] = $this->input->post('email');
        $data['username'] = $this->input->post('username');
        $data['password'] = $this->input->post('password');
        $data['role_id'] = $this->input->post('role_id');



        // mulai edit dari sini
        $this->load->library('ciqrcode'); //pemanggilan library QR CODE

        $config['cacheable']    = true; //boolean, the default is true
        $config['cachedir']     = 'assets/'; //string, the default is application/cache/
        $config['errorlog']     = 'assets/'; //string, the default is application/logs/
        $config['imagedir']     = 'assets/images/qrcode/'; //direktori penyimpanan qr code
        $config['quality']      = true; //boolean, the default is true
        $config['size']         = '1024'; //interger, the default is 1024
        $config['black']        = array(224, 255, 255); // array, default is array(255,255,255)
        $config['white']        = array(70, 130, 180); // array, default is array(0,0,0)
        $this->ciqrcode->initialize($config);

        $image_name = $data['nim'] . '.png'; //buat name dari qr code sesuai dengan nim

        $params['data'] = 'Nim : ' . $data['nim'] . "\n" . 'No Telp : ' . $data['no_telp'] . "\n" . 'Tanggal Lahir :' . $data['tgl_lahir'] . "\n" . 'Username : ' . $data['username'] . "\n" . 'Password :' . $data['password'] . "\n"; //data yang akan di jadikan QR CODE
        $params['level'] = 'H'; //H=High
        $params['size'] = 10;
        $params['savename'] = $config['imagedir'] . $image_name; //simpan image QR CODE ke folder assets/images/
        $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE

        // editan dari kelr disni
        $data['qr_code'] = $params['savename'];

        $this->m_admin->createMhs($data);

        redirect('Admin/tampildataMahasiswa');
    }

    // buat export

    public function ExportExcelIG(){
        $data = array( 'title' => 'Laporan Excel Instagram',
        'user' => $this->m_admin->getAllInstagram());

        log_helper("export", "Export data Instagram");

        $this->load->view('Admin/laporan_excel_IG',$data);
    }

    public function ExportExcelFB(){
        $data = array( 'title' => 'Laporan Excel Facebook',
        'user' => $this->m_admin->getAllFacebook());

        log_helper("export", "Export data Instagram");

        $this->load->view('Admin/laporan_excel_FB',$data);
    }

    public function ExportExcelTWT(){
        $data = array( 'title' => 'Laporan Excel Twitter',
        'user' => $this->m_admin->getAllTwitter());

        log_helper("export", "Export data Instagram");
        
        $this->load->view('Admin/laporan_excel_TWT',$data);
    }

}
