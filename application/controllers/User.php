<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{

    public function index()
    {
        $data['title'] = 'Administrator';

        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();



        $this->load->view('templates/dashboard_header', $data);
        $this->load->view('templates/dashboard_navbar', $data);
        $this->load->view('templates/dashboard_sidebar', $data);
        $this->load->view('user/index', $data);
        $this->load->view('templates/dashboard_footer');
    }
}
