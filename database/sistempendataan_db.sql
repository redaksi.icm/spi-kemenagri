-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 24 Sep 2020 pada 09.55
-- Versi server: 10.4.14-MariaDB
-- Versi PHP: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sistempendataan_db`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_active` int(1) NOT NULL,
  `username` varchar(128) NOT NULL,
  `last_activity` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id`, `nama`, `email`, `password`, `role_id`, `is_active`, `username`, `last_activity`) VALUES
(1, 'Pembimbing Magang', 'admin@uhamka.ac.id', 'admin', 1, 1, 'admin', '2020-08-30 16:18:56'),
(3, 'echa', 'echa@gmail.com', 'echa', 1, 0, 'echa', '2020-08-30 16:18:56'),
(5, 'yudha', 'yudhaadi334@gmail.com', 'yudhaa', 1, 0, 'yudhas', '2020-08-31 05:07:09');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `id_mhs` int(11) NOT NULL,
  `nim` bigint(20) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `alamat` varchar(225) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `jenis_kelamin` varchar(20) NOT NULL,
  `tempat_lahir` varchar(10) NOT NULL,
  `kewarganegaraan` varchar(30) NOT NULL,
  `agama` varchar(10) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `email` varchar(128) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `role_id` int(11) NOT NULL,
  `qr_code` varchar(50) DEFAULT NULL,
  `kd_prodi` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mahasiswa`
--

INSERT INTO `mahasiswa` (`id_mhs`, `nim`, `nama`, `alamat`, `no_telp`, `jenis_kelamin`, `tempat_lahir`, `kewarganegaraan`, `agama`, `tgl_lahir`, `email`, `username`, `password`, `role_id`, `qr_code`, `kd_prodi`) VALUES
(1, 1703015032, 'Yudha Adi Hendrawan Prakoso', 'Jl. Hijau  IV Blok I no.12', '083892903537', 'laki - laki', 'jakarta', 'Indonesia', 'Islam', '1999-07-19', 'yudhaadi@uhamka.ac.id', 'yudha1', '12345', 3, 'assets/images/qrcode/1703015032.png', 'FT_TI_01'),
(2, 1703015031, 'Prakoso Hendrawans', 'Jl.Biru kuning hijau blok IV', '08389290353999', 'laki - laki', 'singapore', 'singapore', 'Islam', '1999-07-19', 'hendrawanPrakoso@gmail.com', 'hendrawan', '1234567', 3, 'assets/images/qrcode/1703015031.png', 'FT_TI_01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_log`
--

CREATE TABLE `tabel_log` (
  `log_id` int(11) NOT NULL,
  `log_user` varchar(255) DEFAULT NULL,
  `log_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `log_tipe` varchar(50) DEFAULT NULL,
  `log_desc` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tabel_log`
--

INSERT INTO `tabel_log` (`log_id`, `log_user`, `log_time`, `log_tipe`, `log_desc`) VALUES
(3, 'echa', '2020-08-30 21:58:15', '3', 'Mengedit data admin'),
(4, 'echa', '2020-08-30 22:08:23', '3', 'Mengedit data admin'),
(5, 'echa', '2020-08-30 22:09:25', '2', 'Menambah data admin'),
(6, 'echa', '2020-08-30 22:13:06', '4', 'Menghapus data admin'),
(7, 'echa', '2020-08-30 22:14:49', '3', 'Mengedit data Twitter'),
(8, 'echa', '2020-08-30 22:15:44', '2', 'Menambah data comment Facebook'),
(9, 'echa', '2020-08-30 22:19:55', '5', 'Export data Instagram'),
(10, 'echa', '2020-08-30 22:46:55', '2', 'Menambah data admin'),
(11, 'echa', '0000-00-00 00:00:00', '3', 'Mengedit data admin'),
(12, 'echa', '2020-08-31 05:07:09', '3', 'Mengedit data admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_facebook_acc`
--

CREATE TABLE `tb_facebook_acc` (
  `id_data_fb` int(11) NOT NULL,
  `account` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `no_telp` varchar(255) NOT NULL,
  `pertanyaan` longtext NOT NULL,
  `tanggapan` longtext NOT NULL,
  `tanggal_comment` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_facebook_acc`
--

INSERT INTO `tb_facebook_acc` (`id_data_fb`, `account`, `alamat`, `no_telp`, `pertanyaan`, `tanggapan`, `tanggal_comment`) VALUES
(1, '@chabellah', 'jl.merah', '082938298392', 'kenapa sering jutek ?', 'udah bawaaanya mas', '2020-07-16 06:30:00'),
(5, '@prakosossssss', 'Jl. Kelapa kuning 9', '083892903537', 'Kenapa sistem down terus ??', 'ya lgi down elah', '2020-08-13 10:15:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_instagram_acc`
--

CREATE TABLE `tb_instagram_acc` (
  `id_data_ig` int(11) NOT NULL,
  `account` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `no_telp` varchar(255) NOT NULL,
  `pertanyaan` longtext NOT NULL,
  `tanggapan` longtext NOT NULL,
  `tanggal_comment` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_instagram_acc`
--

INSERT INTO `tb_instagram_acc` (`id_data_ig`, `account`, `alamat`, `no_telp`, `pertanyaan`, `tanggapan`, `tanggal_comment`) VALUES
(5, '@chabela', 'Jl.biru', '083892903537', 'Kenapa web tidak bisa di buka ya ? terimakasih', 'lagi maintenance mba , maaf ya mba atau mas', '2020-06-09 18:17:00'),
(6, '@yudha', 'jl. merah', '08282832839', 'Kok tidak bisa kirim email ?', 'emailnya sedang di perbaiki mas', '2020-07-20 15:15:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_twitter_acc`
--

CREATE TABLE `tb_twitter_acc` (
  `id_data_twitter` int(11) NOT NULL,
  `account` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `no_telp` varchar(255) NOT NULL,
  `pertanyaan` longtext NOT NULL,
  `tanggapan` longtext NOT NULL,
  `tanggal_comment` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_twitter_acc`
--

INSERT INTO `tb_twitter_acc` (`id_data_twitter`, `account`, `alamat`, `no_telp`, `pertanyaan`, `tanggapan`, `tanggal_comment`) VALUES
(1, '@chachaaaaaaaaaaaaaaaaaa', 'jl.meraahh', '082823293923', 'Kenapa unmood terus ? ', 'kurang nyemil dan kurang makan mas', '2020-07-17 20:32:53'),
(2, '@prakoso', 'jl.merah', '0230293023', 'web down ya ?', 'ya', '2020-07-24 19:28:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `role` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_role`
--

INSERT INTO `user_role` (`id`, `role_id`, `role`) VALUES
(1, 1, 'Administrator'),
(2, 2, 'MahasiswaMagang');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`id_mhs`) USING BTREE;

--
-- Indeks untuk tabel `tabel_log`
--
ALTER TABLE `tabel_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indeks untuk tabel `tb_facebook_acc`
--
ALTER TABLE `tb_facebook_acc`
  ADD PRIMARY KEY (`id_data_fb`);

--
-- Indeks untuk tabel `tb_instagram_acc`
--
ALTER TABLE `tb_instagram_acc`
  ADD PRIMARY KEY (`id_data_ig`);

--
-- Indeks untuk tabel `tb_twitter_acc`
--
ALTER TABLE `tb_twitter_acc`
  ADD PRIMARY KEY (`id_data_twitter`);

--
-- Indeks untuk tabel `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `mahasiswa`
--
ALTER TABLE `mahasiswa`
  MODIFY `id_mhs` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tabel_log`
--
ALTER TABLE `tabel_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `tb_facebook_acc`
--
ALTER TABLE `tb_facebook_acc`
  MODIFY `id_data_fb` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tb_instagram_acc`
--
ALTER TABLE `tb_instagram_acc`
  MODIFY `id_data_ig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tb_twitter_acc`
--
ALTER TABLE `tb_twitter_acc`
  MODIFY `id_data_twitter` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
